package com.joyce;

import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.turbine.EnableTurbine;

@SpringBootApplication
@EnableEurekaClient
@EnableDiscoveryClient
@EnableTurbine
public class TurbineServerApplication {
	private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(TurbineServerApplication.class);

	public static void main(String[] args) { 
		SpringApplication.run(TurbineServerApplication.class, args); 
	}

}
