项目启动顺序：
joyce-eureka-server    //8761
joyce-zipkin-server    //8001
joyce-turbine-server   //8002
joyce-index            //启动两个端口：8010、8011
joyce-user            //启动两个端口：8020、8021

首先访问turbine任何一个监控中的微服务接口：
http://localhost:8010/index?name=kkkk

启动成功之后，在浏览器里输入：
localhost:8002/turbine.stream
如果浏览器不断有json数据输出，表示启动成功。数据长这样 ：
: ping
data: {"reportingHostsLast10Seconds":1,"name":"meta","type":"meta","timestamp":1548165825481}

http://localhost:8001/hystrix.stream 浏览器打开此地址，会不断有ping: 输出
http://localhost:8001  浏览器打开此地址，可以看到zipkin的trace追踪页面，点击find trace按钮，可以看到所有被追踪到的信息。
http://localhost:8001/hystrix 浏览器打开此地址，出来Hystrix Dashboard页面，在长长的输入框里输入localhost:8002/turbine.stream，点击Monitor Stream 
按钮就可以看到微服务监控页面了！

微服务集群监控截图.png   在项目根目录下